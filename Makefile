#.DEFAULT_GOAL := say_goodbye
#Target : ici si on veut parametrer une target pour nos scripts
#.PROC1 := say_goodbye say_hello
.DEFAULT_GOAL := help

# Default make proc if she is in first func postion
all: say_goodbye say_hello say_toto

# Vars
CO = @composer
CO_OPT_V = -V

# Funcs
say_hello: ## dire Hello World
	@echo "Hello World"

say_goodbye: ## dire Goodbye
	@echo "GoodBye"

say_toto: ## dire Toto
	@echo "Toto"
	@$(MAKE) -s say_goodbye
# fonctionne ! $(MAKE) cv
# affiche juste cv, fonctionne ?  @echo ${call cv_f},
# fonctionne pas : ${call cv}

#Déclarer et utiliser une variable pour une cmd, composer version
cv: ## composer -V quand on en a besoin
	${CO} -V

#Condition : si un de ces 3 files changent on exec la cmd
#cvo : composer_v_onchange
cvo: composer.json composer.lock symfony.lock ## composer -V si changement
	${CO} ${CO_OPT_V}
# avant : ${CO} -V
# possible d'appeler une func comme cv ?

deploy: ## deploy with ansistrano
	@ansible-playbook -i ansible/prod ansible/deploy/deploy.yml

deployv: ## deploy with verbose option
	@ansible-playbook -i ansible/prod ansible/deploy/deploy.yml -vvv

deploy_goodbye: deploy say_goodbye ## deploy and say goodbye

# Function pour voir toutes les cmd
# et tous leurs commentaires avec un grep sur ##
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
