<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixture extends Fixture
{
    private $encoder;
    public const USER_REFERENCE = 'user';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //On se crée 9 users classiques
        for ($x = 1; $x < 10; $x++) {
            $user = new User();
            $user->setUsername('user' . $x);
            $user->setPassword($this->encoder->encodePassword($user, '12345'));
            $user->setRole('ROLE_USER');
            $manager->persist($user);
            //On ajoute chaque user en référence pour d'autres fixtures
            $this->addReference(self::USER_REFERENCE.$x, $user);
        }
        //On se crée un user admin
        $user = new User();
        $user->setUsername('admin');
        $user->setPassword($this->encoder->encodePassword($user, 'admin'));
        $user->setRole('ROLE_ADMIN');
        $manager->persist($user);

        $manager->flush();
    }
}
