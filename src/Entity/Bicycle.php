<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\BicycleRepository")
 */
class Bicycle {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $brand;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $color;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $gearNb;

    /**
     * @ORM\Column(type="boolean")
     */
    public $electric;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): string
    {
        $this->color = $color;

        return $this->color;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): string
    {
        $this->brand = $brand;

        return $this->brand;
    }

    public function getGearNb()
    {
        return $this->gearNb;
    }

    public function setGearNb($gearNb)
    {
        $this->gearNb = $gearNb;

        return $this->gearNb;
    }

    public function getElectric(): ?bool
    {
        return $this->electric;
    }

    public function setElectric(?bool $electric): bool
    {
        $this->electric = $electric;

        return $this->electric;
    }

}