<?php

namespace App\Repository;

use App\Entity\Bicycle;

/**
 * On fait ici une classe de type DAO (Data Access Object) dont l'objectif
 * sera exclusivement de faire le lien entre la base de données et le PHP.
 * Il n'y a que dans ces classes qu'on verra du SQL, des informations sur
 * la bdd et autre, le reste de l'application manipulera uniquement des Entity.
 * Pour ce DAO, on utilise la librairie PDO de PHP qui permet de se
 * connecter et faire des requêtes sur des bases de données SQL.
 * A terme, cette classe contiendra les 5 méthodes du CRUD.
 */
class BicycleRepository {

    /**
     * Méthode permettant de récupérer l'intégralité des vélos sur la bdd
     * @return Bicycle[] La liste des vélos
     */
    public function findAll():array {
        $bikes = [];


        $connection = ConnectionUtil::getConnection();
        
        //On utilise la connexion pour préparer une requête SQL, ici un SELECT
        $query = $connection->prepare("SELECT * FROM bicycle");
        //On exécute la requête préparée
        $query->execute();
        /**
         * On boucle sur les résultats de la requête qui nous sont renvoyés
         * sous forme de tableau de tableau associatif. Le but est de transformer
         * ces tableaux associatifs en instance de la classe Bicycle
         */
        $results = $query->fetchAll();
        foreach ($results as $line) {
           //On appel la méthode sqlToBike pour récupérer une
           //instance de bike correspondant à la ligne de résultat
           //actuelle et on push cette instance dans le tableau
            $bikes[] = $this->sqlToBike($line);
            //array_push($bikes, $bike);
        }
        //On return le tableau en question
        return $bikes;
    }

    /**
     * Méthode qui va prendre une instance de Bicycle et faire une
     * requête SQL pour la faire persister en base de données
     */
    public function add(Bicycle $bike): void {
        //On récupère notre connexion PDO avec notre classe utilitaire
        $connection = ConnectionUtil::getConnection();
        /**
         * On prépare la requête mais cette fois ci, comme on a des data
         * à mettre dans celle ci, on met des placeholder (les trucs qui
         * commencent par :) là où on voudra mettre des paramètres
         */
        $query = $connection->prepare("INSERT INTO bicycle (brand,color,gear_nb,electric) VALUES (:brand,:color,:gearNb,:electric)");
        /**
         * Pour assigner des valeurs aux placeholders de notre requête
         * on utilise la méthode bindValue qui attend en premier le
         * nom du placeholder et en deuxième la valeur à lui assigner,
         * et optionnelement en troisième le type de donnée attendu
         * par la colonne de la table SQL
         */
        $query->bindValue(":brand", $bike->brand, \PDO::PARAM_STR);
        $query->bindValue(":color", $bike->color, \PDO::PARAM_STR);
        $query->bindValue(":gearNb", $bike->gearNb, \PDO::PARAM_INT);
        $query->bindValue(":electric", $bike->electric, \PDO::PARAM_BOOL);
        $query->execute();

        /**
         * On récupère l'id que MySQL a généré lors de l'ajout et on
         * l'assigne à la propriété id de notre instance, comme ça on
         * se retrouve à avoir un Bicycle complet (avec id) une fois
         * qu'on aura fait le add
         */
        $bike->id = $connection->lastInsertId();
    }
    /**
     * Méthode permettant d'aller chercher une instance spécifique 
     * de Bicycle selon son id.
     * Pour le typage de retour, on met un "?" en plus pour dire que
     * la méthode renverra soit une instance de Bicycle, soit null,
     * c'est une syntaxe PHP. On fait ça pour faire en sorte que 
     * si on donne un id incorrect, on ne récupère pas un Bicycle
     * sans valeur mais directement null
     */
    public function find(int $id): ?Bicycle {
        $connection = ConnectionUtil::getConnection();
        /**
         * On prépare la requête et on assigne les value aux
         * placeholders
         */
        $query = $connection->prepare("SELECT * FROM bicycle WHERE id=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

        /**
         * Ici, syntaxe un peu particulière, on fait à la fois une 
         * assignation et un test à l'intérieur d'un if.
         * En réalité ça va se faire en plusieurs temps : le 
         * $query->fetch() va s'exécuter, on va assigner le retour de
         * ce fetch() à la variable $line, et enfin le if va vérifier
         * est-ce que la variable $line est true ou false
         * Le $query->fetch() est une méthode qui récupère la ligne
         * suivante de résultat sous forme de tableau associatif ou
         * qui renvoit false si on est arrivé au bout des lignes
         */
        if($line = $query->fetch()) {
            //On return le Bicycle en se servant de la méthode si 
            //résultat il y a
            return $this->sqlToBike($line);

        }
        //On return null si on est pas passé par le if
        return null;
    }

    /**
     * Méthode qui va mettre à jour les valeurs d'un Bicycle donné.
     * L'argument attendu est donc un Bicycle avec un id pour pouvoir
     * le modifier
     */
    public function update(Bicycle $bike): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("UPDATE bicycle SET brand=:brand, color=:color,gear_nb=:gear_nb, electric=:electric WHERE id=:id");
        $query->bindValue(":brand", $bike->brand, \PDO::PARAM_STR);
        $query->bindValue(":color", $bike->color, \PDO::PARAM_STR);
        $query->bindValue(":gear_nb", $bike->gearNb, \PDO::PARAM_INT);
        $query->bindValue(":electric", $bike->electric, \PDO::PARAM_BOOL);
        $query->bindValue(":id", $bike->id, \PDO::PARAM_INT);

        $query->execute();

    }

    /**
     * Méthode qui va supprimer un Bicycle en se basant sur son id
     * L'argument attendu ici est un Bicycle mais s'il n'a qu'un id,
     * c'est suffisant pour que la méthode fonctionne
     */
    public function remove(Bicycle $bike): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("DELETE FROM bicycle WHERE id=:id");
        $query->bindValue(":id", $bike->id, \PDO::PARAM_INT);

        $query->execute();
    }

    /**
     * Méthode qui prend une ligne de résultat PDO en argument (un 
     * tableau associatif) et qui, à partir de celle ci, crée une
     * instance de Bicycle avant de la return.
     * Cette méthode ne sert qu'à éviter les répétition vu que c'est
     * une opération qu'on fait à plusieurs reprises
     */
    private function sqlToBike(array $line): Bicycle {
        //On crée l'instance
        $bike = new Bicycle();
        //On assigne les valeurs de la ligne de résultat aux 
        //différentes propriétés de notre classe (c'est ici qu'on va
        //convertir les données qui doivent l'être etc.)
        $bike->id = intval($line["id"]);
        $bike->brand = $line["brand"];
        $bike->color = $line["color"];
        $bike->gearNb = intval($line["gear_nb"]);
        $bike->electric = boolval($line["electric"]);
        //On renvoie l'instance
        return $bike;
    }
}