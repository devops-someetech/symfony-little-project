<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\BicycleType;
use App\Entity\Bicycle;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\BicycleRepository;


class BikeController extends AbstractController {

    //Ici, on injecte notre DAO dans notre Route afin de pouvoir utiliser
    //ses méthodes d'accès aux données
    /**
     * @Route("/", name="add_bike")
     */
    public function index(Request $request, BicycleRepository $repo) {
        
        // dump($repo->find(100));

        // $bike = $repo->find(1);
        // $bike->brand = "follis";
        // $repo->update($bike);
        // $repo->remove($bike);
        
        $bicycle = new Bicycle();
        $form = $this->createForm(BicycleType::class, $bicycle);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            /**
             * Si le formulaire est submit et valide, alors on déclenche
             * la méthode add de notre repository pour faire persister
             * l'instance de Bicycle liée à notre formulaire
             */
            $repo->add($bicycle);
            dump($bicycle);
        }
        
        return $this->render("add-bike.twig.html", [
            "form" => $form->createView(),
            "bike" => $bicycle
        ]);
    }
}